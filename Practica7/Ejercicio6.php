<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 7 Ejercicio 6</title>
    </head>
    <body>
        <p>Programa que suma valores impares y los imprime.</p>
        <?php
        $acumulador = [];
        $resultado = 0;
        $c = 30;

        echo "<div style='width: 600px'>";

        for ($i = 1; $i < $c; $i += 2) {
            array_push($acumulador, $i);
            $resultado += $i;
        }

        echo implode('+', $acumulador) . " = " . $resultado;
        echo "</div>";
        ?>
    </body>
</html>
