<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 7 Ejercicio 3</title>
    </head>
    <body>
        <p>Ciclo for que nos muesta numeros del 1 al 150, cada numero en un div diferente.</p>
        <?php
        for ($c = 1; $c <= 150; $c++) {
            echo "<div>$c</div>";
        }
        ?>
    </body>
</html>
