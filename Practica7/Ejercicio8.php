<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 7 Ejercicio 8</title>
    </head>
    <body>
        <p>Programa que calcula la media entre los valores de un array.</p>
        <?php
        $numeros = [10, 20, 30, 40];
        $imprimir = implode(", ", $numeros);
        $suma = 0;
        $acumulador = 0;

        foreach ($numeros as $value) {
            $suma += $value;
            $acumulador++;
        }

        $resultado = $suma / $acumulador;

        echo "<div>Numeros del array: $imprimir</div>";
        echo "<div>Media de los valores: $resultado</div>";
        ?>
    </body>
</html>
