<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 7 Ejercicio 5</title>
    </head>
    <body>
        <p>Ciclo while que nos muesta numeros del 1 al 150, cada numero en un div diferente.</p>
            <?php
            $c = 1;

            while ($c <= 150) {
                echo "<div>$c</div>";
                $c++;
            }
            ?>
    </body>
</html>
