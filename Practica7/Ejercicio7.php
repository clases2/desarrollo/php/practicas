<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 7 Ejercicio 7</title>
    </head>
    <body>
        <p>Programa que imprime numeros de un array con for y foreach.</p>
        <?php
        $numeros = [1, 2, 3, 4];

        echo "<div>Bucle for:<br>";
        for ($i = 0; $i < count($numeros); $i++) {
            echo "<p>$numeros[$i]</p>";
        }
        echo "</div>";

        echo "<div>Bucle foreach:<br>";
        foreach ($numeros as $value) {
            echo "<p>$value</p>";
        }
        echo "</div>";
        ?>
    </body>
</html>
