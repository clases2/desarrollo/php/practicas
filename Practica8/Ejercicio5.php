<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 8 Ejercicio 5</title>
    </head>
    <body>
        <form action="Ejercicio5_Destino.php">
            <div><label for="nombre">Nombre: </label><input type="text" name="nombre" id="nombre" required="" placeholder="Introduce tu nombre." /></div>
            <div><label for="apellido">Apellido: </label><input type="text" name="apellido" id="apellido" required="" placeholder="Introduce tu apellido." /></div>
            <div><label for="postal">Codigo Postal: </label><input type="text" name="postal" id="postal" required="" placeholder="Introduce tu codigo postal." /></div>
            <div><label for="telefono">Telefono: </label><input type="number" name="telefono" id="telefono" required="" placeholder="Introduce tu numero de telefono." /></div>
            <div><label for="correo">Correo: </label><input type="email" name="correo" id="correo" required="" placeholder="Introduce tu Email." /></div>
            <button>Enviar</button>
        </form>
    </body>
</html>
