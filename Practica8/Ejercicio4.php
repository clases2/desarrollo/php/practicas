<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 8 Ejercicio 4</title>
    </head>
    <body>
        <form action="Ejercicio4_Destino.php">
            <div><label for="nombre">Nombre: </label><input type="text" name="nombre" id="nombre" placeholder="Introduce tu nombre." /></div>
            <div><label for="correo">Email: </label><input type="text" name="correo" id="correo" placeholder="Introduce tu correo." /></div>
            <div><button formmethod="post">Enviar con POST</button><button formmethod="get">Enviar con GET</button></div>
        </form>
    </body>
</html>
