<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 8 Ejercicio 3</title>
    </head>
    <body>
        <form action="Ejercicio3_Destino.php" method="post">
            <div><label for="nombre">Nombre: </label><input type="text" name="nombre" id="nombre" placeholder="Introduce tu nombre." /></div>
            <div><label for="correo">Email: </label><input type="text" name="correo" id="correo" placeholder="Introduce tu correo." /></div>
            <div><label for="telefono">Telefono: </label><input type="tel" name="telefono" id="telefono" placeholder="Introduce tu numero de telefono." /></div>
            <button value="enviar">Enviar</button>
        </form>
    </body>
</html>
