<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 9 Ejercicio 2</title>
    </head>
    <body>
        <?php
        $numero = "";
        if (!isset($_POST["datos"])) {
            include './Ejercicio2_Vista.php';
        } else {
            extract($_POST);
            include './Ejercicio2_Vista.php';
            if ($numero >= 0) {
                echo "El numero es positivo.";
            } else {
                echo "El numero es negativo.";
            }
        }
        ?>
    </body>
</html>
