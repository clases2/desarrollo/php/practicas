<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 9 Ejercicio 1</title>
        <style>
            td{
                border: 1px black solid;
                gap: 0px;
            }
        </style>
    </head>
    <body>
        <?php
        if (!isset($_POST["datos"])) {
            include './Ejercicio1_Vista.php';
        } else {
            extract($_POST);
            ?>

            <table>
                <tr>
                    <td>Nombre: </td>
                    <td><?= $nombre ?></td>
                </tr>
                <tr>
                    <td>Color :</td>
                    <td><?= $color ?></td>
                </tr>
                <tr>
                    <td>Alto :</td>
                    <td><?= $altura ?></td>
                </tr>
                <tr>
                    <td>Peso :</td>
                    <td><?= $peso ?></td>
                </tr>
            </table >
            <?php
        }
        ?>
    </body>
</html>
