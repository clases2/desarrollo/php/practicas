<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 9 Ejercicio 4</title>
    </head>
    <body>
        <?php
        $numero = "";
        if (!isset($_POST["datos"])) {
            include './Ejercicio4_Vista.php';
        } else {
            extract($_POST);
            include './Ejercicio4_Vista.php';
            if ($numero % 2 == 0) {
                echo "El numero es par.";
            } else {
                echo "El numero es impar.";
            }
        }
        ?>
    </body>
</html>
