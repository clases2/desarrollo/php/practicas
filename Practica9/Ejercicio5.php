<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 9 Ejercicio 5</title>
    </head>
    <body>
        <?php
        if (!isset($_POST["datos"])) {
            include './Ejercicio5_Vista.php';
        } else {
            extract($_POST);
            $string = implode(" ", $numeros);
            include './Ejercicio5_Vista.php';
            echo "Entre los numeros $string el mas alto es: " . max($numeros);
        }
        ?>
    </body>
</html>
