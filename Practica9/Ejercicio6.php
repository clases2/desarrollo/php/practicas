<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 9 Ejercicio 6</title>
    </head>
    <body>
        <?php
        if (!isset($_POST["datos"])) {
            include './Ejercicio6_Vista.php';
        } else {
            extract($_POST);
            $string = implode("+", $numeros);
            $resultado = 0;
            foreach ($numeros as $value) {
                $resultado += $value;
            }
            include './Ejercicio6_Vista.php';
            echo $string . ":" . $resultado;
        }
        ?>
    </body>
</html>
