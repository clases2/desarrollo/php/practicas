<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 9 Ejercicio 7</title>
    </head>
    <body>
        <?php
        if (!isset($_POST["datos"])) {
            include './Ejercicio7_Vista.php';
        } else {
            extract($_POST);
            $suma = implode("+", $numeros);
            $rsuma = 0;
            foreach ($numeros as $value) {
                $rsuma += $value;
            }
            $producto = implode("x", $numeros);
            $rproducto = 1;
            foreach ($numeros as $value) {
                $rproducto *= $value;
            }

            //Desde aqui el foreach no va a servir asi que uso un for.
            $resta = implode("-", $numeros);
            $rresta = $numeros[0];
            for ($i = 1; $i < count($numeros); $i++) {
                $rresta -= $numeros[$i];
            }
            $division = implode("/", $numeros);
            $rdivision = $numeros[0];
            for ($i = 1; $i < count($numeros); $i++) {
                $rdivision /= $numeros[$i];
            }

            include './Ejercicio7_Vista.php';
            echo $suma . ":" . $rsuma . "<br>";
            echo $resta . ":" . $rresta . "<br>";
            echo $producto . ":" . $rproducto . "<br>";
            echo $division . ":" . $rdivision;
        }
        ?>
    </body>
</html>
