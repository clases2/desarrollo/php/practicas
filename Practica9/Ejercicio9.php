<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 9 Ejercicio 9</title>
    </head>
    <body>
        <?php
        if (!isset($_POST["datos"])) {
            include './Ejercicio9_Vista.php';
        } else {
            extract($_POST);
            include './Ejercicio9_Vista.php';
            if ($numero <= 3) {
                echo "La nota $numero es muy deficiente.";
            } else if ($numero > 3 && $numero <= 5) {
                echo "Con la nota $numero estas suspenso.";
            } else if ($numero > 5 && $numero <= 6) {
                echo "La nota $numero es suficiente.";
            } else if ($numero > 6 && $numero <= 7) {
                echo "Con la nota $numero vas bien.";
            } else if ($numero > 7 && $numero < 9) {
                echo "La nota $numero es notable.";
            } else if ($numero >= 9 && $numero <= 10) {
                echo "La nota $numero es sobresaliente.";
            }
        }
        ?>
    </body>
</html>
