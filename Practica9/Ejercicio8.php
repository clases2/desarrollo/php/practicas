<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 9 Ejercicio 8</title>
    </head>
    <body>
        <?php
        if (!isset($_POST["datos"])) {
            include './Ejercicio8_Vista.php';
        } else {
            extract($_POST);
            include './Ejercicio8_Vista.php';
            $longitud = (M_PI * 2) * $numero;
            $area = M_PI * ($numero ** 2);
            $volumen = (4 / 3) * M_PI * ($numero ** 3);
            echo "Si el radio del circulo es $numero:<br>";
            echo "Longitud: $longitud:<br>";
            echo "Area: $area:<br>";
            echo "Volumen: $volumen";
        }
        ?>
    </body>
</html>
