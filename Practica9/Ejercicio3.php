<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 9 Ejercicio 3</title>
    </head>
    <body>
        <?php
        $letra = "";
        if (!isset($_POST["datos"])) {
            include './Ejercicio3_Vista.php';
        } else {
            extract($_POST);
            strtolower($letra);
            include './Ejercicio3_Vista.php';
            switch ($letra) {
                case "a":
                    echo "La letra introducida es una vocal";
                    break;
                case "e":
                    echo "La letra introducida es una vocal";
                    break;
                case "i":
                    echo "La letra introducida es una vocal";
                    break;
                case "o":
                    echo "La letra introducida es una vocal";
                    break;
                case "u":
                    echo "La letra introducida es una vocal";
                    break;
                default:
                    echo "La letra introducida no es una vocal";
                    break;
            }
        }
        ?>
    </body>
</html>
