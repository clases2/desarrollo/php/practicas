<?php
if (empty($_REQUEST)) {
    $mal = true;
    $error = "Introduce los datos en el formulario.";
} else if (empty($_REQUEST["numero"])) {
    $mal = true;
    $error = "El nmero es obligatorio.";
} else if ($_REQUEST["numero"] < 0) {
    $mal = true;
    $error = "El numero introducido debe ser mayor que 0.";
} else {
    $mal = false;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 6 Ejercicio 2</title>
    </head>
    <body>
        <?php
        if (!$mal) {
            var_dump($_REQUEST);
        } else {
            echo $error;
            ?>
            <div>
                <form name="f">
                    numero <input type="number" name="numero" value="" />
                    <input type="submit" value="Enviar" name="boton" />
                </form>
            </div>
            <?php
        }
        ?>
    </body>
</html>
