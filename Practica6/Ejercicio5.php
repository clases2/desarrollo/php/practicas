<?php
if ($_REQUEST) {
    $mal = false;
} else {
    $mal = true;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 6 Ejercicio 5</title>
    </head>
    <body>
        <?php
        if ($mal) {
            ?>
            <form name="f">
                <select multiple name="ciudad[]">
                    <option value="SS">Santander</option>
                    <option value="PA">Palencia</option>
                    <option value="PO">Potes</option>
                </select>
                <input type="submit" value="Enviar" name="boton" />
            </form>
            <?php
        } else {
            $ciudad = $_REQUEST['ciudad'];
            $ciudades = [
                "SS" => "Santander",
                "PA" => "Palencia",
                "PO" => "Potes"
            ];
            foreach ($ciudad as $value) {
                echo "$value-" . $ciudades[$value] . "<br>";
            }
        }
        ?>
    </body>
</html>
