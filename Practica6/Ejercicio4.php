<?php
if ($_REQUEST) {
    $mal = false;
} else {
    $mal = true;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 6 Ejercicio 4</title>
    </head>
    <body>
        <?php
        if ($mal) {
            ?>
            <form name="f">
                <select name="ciudad">
                    <option value="SS">Santander</option>
                    <option value="PA">Palencia</option>
                    <option value="PO">Potes</option>
                </select>
                <input type="submit" value="Enviar" name="boton" />
            </form>
            <?php
        } else {
            $ciudades = [
                "SS" => "Santander",
                "PA" => "Palencia",
                "PO" => "Potes"];
            echo $ciudades[$_REQUEST['ciudad']];
        }
        ?>
    </body>
</html>
