<?php
if ($_REQUEST) {
    $mal = false;
} else {
    $mal = true;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 6 Ejercicio 9</title>
    </head>
    <body>
        <?php
        if ($mal) {
            ?>
            <form name="f">
                <legend>
                    Seleccione la ciudad
                </legend>

                <select name="ciudad" id="ciudad">
                    <optgroup label="Asia">
                        <option value="3">Delhi</option>
                        <option value="4">Hong Kong</option>
                        <option value="8">Mumbai</option>
                        <option value="11">Tokyo</option>
                    </optgroup>
                    <optgroup label="Europe">
                        <option value="1">Amsterdam</option>
                        <option value="5">London</option>
                        <option value="7">Moscu</option>
                    </optgroup>
                    <optgroup label="North America">
                        <option value="6">Log Angeles</option>
                        <option value="9">New York</option>
                    </optgroup>
                    <optgroup label="South America">
                        <option value="2">Buenos Aires</option>
                        <option value="10">Sao Paulo</option>
                    </optgroup>
                </select>

                <input type="submit" value="Enviar" name="boton" />
            </form>
            <?php
        } else {
            $ciudades = ["", "Amsterdam", "Buenos Aires", "Delhi", "Hong Kong", "London", "Los Angeles", "Moscu", "Mumbai", "New York", "Sao Paulo", "Tokyo"];
            var_dump($_REQUEST['ciudad']);
            echo "El elemento seleccionados es: " . $ciudades[$_REQUEST['ciudad']] . ".";
        }
        ?>
    </body>
</html>
