<?php
if ($_REQUEST) {
    $mal = false;
} else {
    $mal = true;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 6 Ejercicio 11</title>
        <style>
            body{
                width: 100%;
            }
            td, table{
                border:1px black solid;
                border-collapse: collapse;
            }
            form{
                margin: 0 auto;
                text-align: center;
            }
            table{
                margin: 20px auto;
                width: 500px;
            }
            td{
                padding: 10px;
            }
            tr td:first-child{
                background-color: darkseagreen;
            }
            tr td:nth-child(2){
                background-color: gainsboro;
                text-align: justify;
            }
        </style>
    </head>
    <body>
        <?php
        if ($mal) {
            ?>
            <form name="f">
                <h1>Formulario de inscripcion de usuarios.</h1>
                <table>
                    <tr>
                        <td>Nombre completo</td>
                        <td><input type="text" name="nombre" /></td>
                    </tr>
                    <tr>
                        <td>Direccion</td>
                        <td><input type="text" name="direccion" /></td>
                    </tr>
                    <tr>
                        <td>Correo Electronico</td>
                        <td><input type="email" name="email" /></td>
                    </tr>
                    <tr>
                        <td>Contraseña</td>
                        <td><input type="text" name="contraseña1" /></td>
                    </tr>
                    <tr>
                        <td>Confirmar<br>Contraseña<br><br>Fecha de<br>Nacimiento</td>
                        <td><input type="text" name="contraseña2" /><br><br><br><input type="date" name="fecha[]" /></td>
                    </tr>
                    <tr>
                        <td>
                            Sexo<br><br>
                            For favor elige<br>
                            los temas de tus<br>
                            intereses
                        </td>
                        <td>
                            <input type="radio" value="hombre" id="hombre" name="sexo" /><label for="sexo">Hombre</label>
                            <input type="radio" value="mujer" id="mujer" name="sexo" /><label for="sexo">Mujer</label><br><br>
                            <input type="checkbox" value="ficcion" id="ficcion" name="intereses[]" /><label for="ficcion">Ficcion</label>
                            <input type="checkbox" value="terror" id="terror" name="intereses[]" /><label for="terror">Terror</label><br>
                            <input type="checkbox" value="accion" id="accion" name="intereses[]" /><label for="accion">Accion</label>
                            <input type="checkbox" value="comedia" id="comedia" name="intereses[]" /><label for="comedia">Comedia</label><br>
                            <input type="checkbox" value="suspenso" id="suspenso" name="intereses[]" /><label for="suspenso">Suspenso</label>
                        </td>
                    </tr>
                    <tr>
                        <td>Selecciona tus<br>aficciones<br><br>(Selecciona multiples<br>elementos pulsando la<br>tecla Control y<br>haciendo click en cada<br>uno, uno a uno.)</td>
                        <td>
                            <select multiple name="aficciones[]" id="aficciones">
                                <option value="Deportes al aire libre">Deportes al aire libre</option>
                                <option value="Deportes de aventuras">Deportes de aventuras</option>
                                <option value="Musica Pop">Musica Pop</option>
                                <option value="Musica Rock">Musica Rock</option>
                                <option value="Musica alternativa">Musica alternativa</option>
                                <option value="Fotografia">Fotografia</option>
                            </select>
                        </td>
                    </tr>
                </table>
                <input type="submit" value="Enviar" name="boton" />
            </form>
            <?php
        } else {
            $nombre = $_GET['nombre'];
            $direccion = $_GET['direccion'];
            $correo = $_GET['email'];
            $contraseña1 = $_GET['contraseña1'];
            $contraseña2 = $_GET['contraseña2'];
            $fecha = $_GET['fecha'];
            $sexo = $_GET['sexo'];
            $intereses = $_GET['intereses'];
            $aficciones = $_GET['aficciones'];

            echo "Datos recogidos: <br>
            Nombre: $nombre.<br>
            Direccion: $direccion.<br>
            Correo electronico: $correo.<br>
            Contraseña: $contraseña1. Confirmacion de contraseña: $contraseña2.<br>
            Fecha: $fecha[0]<br>
            Sexo: $sexo.<br>
            Intereses: ";
            foreach ($intereses as $value) {
                echo "$value ";
            }
            echo ".<br>
            Aficciones: ";
            foreach ($aficciones as $value) {
                echo "$value ";
            }
            echo ".";
        }
        ?>
    </body>
</html>
