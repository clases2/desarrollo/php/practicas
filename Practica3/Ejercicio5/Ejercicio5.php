<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 3 Ejercicio 5</title>
    </head>
    <body>
        <form method='POST'>
            Escribe un numero y el programa determinara si es par o impar: <input type='number' name='caja1'> 
            <input type='submit' value='guardar'>
        </form>
        <?php
        if ($_POST) {
            $numero = $_POST['caja1'];
            if ($numero % 2 == 0) {
                echo "El numero es par";
            } else {
                echo "El numero es impar";
            }
        }
        ?>
    </body>
</html>
