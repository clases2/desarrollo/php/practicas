<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 3 Ejercicio 12</title>
    </head>
    <body>
        <form method='POST'>
            Escribe tres numeros y el programa calculara una hora con minutos y segundos y le sumara un segudo. <br>
            <input type="number" name="numero1" placeholder="hh" min="0">
            <input type="number" name="numero2" placeholder="mm" min="0" max="59">
            <input type="number" name="numero3" placeholder="ss" min="0" max="59">
            <button type="submit">Calcular</button>
        </form>
        <?php
        if ($_POST) {
            $n1 = $_POST['numero1'];
            $n2 = $_POST['numero2'];
            $n3 = $_POST['numero3'] + 1;

            if ($n3 == 60) {
                $n3 = 0;
                $n2++;
            }

            if ($n2 == 60) {
                $n2 = 0;
                $n1++;
            }

            $hora = $n1 . 'h: ' . $n2 . 'min: ' . $n3 . 's';
            echo $hora;
        }
        ?>
    </body>
</html>
