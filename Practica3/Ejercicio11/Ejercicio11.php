<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 3 Ejercicio 11</title>
    </head>
    <body>
        <form method='POST'>
            Escribe una nota para un alumno y el programa determinara su desempeño. <br>
            <input type="number" name="nota">
            <button type="submit">Calcular</button>
        </form>
        <?php
        if ($_POST) {
            $nota = $_POST['nota'];

            if ($nota <= 3) {
                echo "Muy deficiente.";
            } elseif ($nota > 3 && $nota <= 5) {
                echo "Suspenso.";
            } elseif ($nota == 6) {
                echo "Suficiente.";
            } elseif ($nota == 7) {
                echo "Bien.";
            } elseif ($nota >= 8 && $nota <= 9) {
                echo "Notable.";
            } else {
                echo "Sobresaliente.";
            }
        }
        ?>
    </body>
</html>
