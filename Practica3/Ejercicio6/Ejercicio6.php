<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 3 Ejercicio 6</title>
    </head>
    <body>
        <form method='POST'>
            Escribe tres numeros y el programa imprimira el menor de ellos. <br>
            <input type="number" name="numero1">
            <input type="number" name="numero2">
            <input type="number" name="numero3">
            <button type="submit">Calcular</button>
        </form>
        <?php
        if ($_POST) {

            $n1 = $_POST['numero1'];
            $n2 = $_POST['numero2'];
            $n3 = $_POST['numero3'];
            $menor = 0;

            if ($n1 < $n2 && $n1 < $n3) {
                $menor = $n1;   
            } elseif ($n2 < $n1 && $n2 < $n3) {
                $menor = $n2;
            } else {
                $menor = $n3;
            }
            echo "El menor es: $menor";
        }
        ?>
    </body>
</html>
