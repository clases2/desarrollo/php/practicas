<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 3 Ejercicio 9</title>
    </head>
    <body>
        <form method='POST'>
            Escribe dos numeros y el programa hara varias operaciones. <br>
            <input type="number" name="numero1">
            <input type="number" name="numero2">
            <button type="submit">Calcular</button>
        </form>
        <?php
        if ($_POST) {
            $n1 = $_POST['numero1'];
            $n2 = $_POST['numero2'];

            $suma = $n1 + $n2;
            $resta = $n1 - $n2;
            $producto = $n1 * $n2;
            $division = $n1 / $n2;

            echo "$n1 + $n2 = $suma<br>";
            echo "$n1 - $n2 = $resta<br>";
            echo "$n1 x $n2 = $producto<br>";
            echo "$n1 / $n2 = $division";
        }
        ?>
    </body>
</html>
