<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 3 Ejercicio 10</title>
    </head>
    <body>
        <form method='POST'>
            Escribe un radio y el programa calculara la longitud, area y volumen de la esfera. <br>
            <input type="number" name="radio">
            <button type="submit">Calcular</button>
        </form>
        <?php
        if ($_POST) {
            $radio = $_POST['radio'];

            $longitud = (2 * M_PI) * $radio;
            $area = M_PI * pow($radio, 2);
            $volumen = (4 / 3 * M_PI) * pow($radio, 3);

            echo"Radio: $radio.<br>";
            echo"Longitud: $longitud.<br>";
            echo"Area: $area.<br>";
            echo"Volumen: $volumen.<br>";
        }
        ?>
    </body>
</html>
