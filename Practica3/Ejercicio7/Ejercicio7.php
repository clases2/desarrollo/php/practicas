<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 3 Ejercicio 7</title>
    </head>
    <body>
        <form method='POST'>
            Escribe dos numeros y el programa imprimira el resultado de su suma. <br>
            <input type="number" name="numero1">
            <input type="number" name="numero2">
            <button type="submit">Sumar</button>
        </form>
        <?php
        if ($_POST) {
            $n1 = $_POST['numero1'];
            $n2 = $_POST['numero2'];
            $n3 = $n1 + $n2;
            echo "$n1 + $n2 = $n3";
        }
        ?>
    </body>
</html>
