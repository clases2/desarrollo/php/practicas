<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 10 Ejercicio 3</title>
        <script>
            window.addEventListener("load", inicio);
            function inicio() {
                document.querySelector("#linea").addEventListener("click", function () {
                    location.reload();
                });
            }
        </script>
    </head>
    <body>
        <?php
        $numero = rand(10, 1000);
        ?>
        <p>Haz click en la linea para cambiar su tamaño.</p>
        <p>Longitud: <?= $numero ?></p>
        <svg width="<?= $numero ?>" height="10" id="linea">
        <rect width="<?= $numero ?>" height="10" style="fill:rgb(0,0,255);stroke-width:10;stroke:rgb(0,0,0)" />
        </svg>
    </body>
</html>
