<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 10 Ejercicio 1</title>
    </head>
    <body>
        <?php
        $numero = rand(10, 1000);
        ?>
        <p>Longitud: <?= $numero ?></p>
        <svg width="<?= $numero ?>" height="10">
        <rect width="<?= $numero ?>" height="10" style="fill:rgb(0,0,255);stroke-width:10;stroke:rgb(0,0,0)" />
        </svg>
    </body>
</html>
