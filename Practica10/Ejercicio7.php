<!DOCTYPE html>

<html>

<head>
    <meta charset="UTF-8">
    <title>Practica 10 Ejercicio 7</title>
    <style type="text/css">
        #circulo {
            visibility: visible;
            position: relative;
            top: 0px;
            left: 0px;
        }
    </style>
    <script type="text/javascript">
        window.addEventListener("load", function() {
            var circulo = document.querySelector("#circulo");
            var x = document.querySelector("#x");
            var y = document.querySelector("#y");
            var boton = document.querySelector("#boton");

            boton.addEventListener("click", function() {
                circulo.style.top = x.value + "px";
                circulo.style.left = y.value + "px";
            });

            circulo.addEventListener("click", function() {
                circulo.style.visibility = "hidden";
            });

        });
    </script>
</head>

<body>
    <?php

    function color()
    {
        $r = rand(0, 255);
        $g = rand(0, 255);
        $b = rand(0, 255);
        $color = "rgb($r,$g,$b)";
        return $color;
    }
    ?>
    <div>
        <p>Escribe dos coordenadas y al presionar el boton el circulo se movera.</p>
        <div><label for="x">X: </label><input type="number" id="x" /><label for="y">Y: </label><input type="number" id="y" /></div>
        <button type="button" id="boton">Mover</button>
    </div>
    <svg height="100" width="100" id="circulo">
        <circle cx="50" cy="50" r="40" stroke="black" stroke-width="3" fill="<?= color() ?>" />
    </svg>
</body>

</html>