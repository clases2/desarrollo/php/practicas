<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 10 Ejercicio 4</title>
        <script>
            window.addEventListener("load", inicio);
            function inicio() {
                document.querySelector("#linea").addEventListener("click", salta);
                function salta(e) {
                    var valor = "";
                    valor = Math.random() * (100 - 1) + 1;
                    e.target.setAttribute("x2", valor);
                }
            }
        </script>
    </head>
    <body>
        <?php
        $longitud = rand(10, 100);
        ?>
        <p>Haz click en la linea para cambiar su tamaño.</p>
        <p>Longitud: <?= $longitud ?></p>
        <svg width="<?= $longitud ?>" height="10">
        <line x1="1" y1="5" x2="<?= $longitud ?>" y2="5" stroke="red" stroke-width="10" id="linea"></line>
        </svg>
    </body>
</html>
