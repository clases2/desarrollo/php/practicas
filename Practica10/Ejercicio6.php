<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 10 Ejercicio 6</title>
    </head>
    <body>
        <?php

        function color() {
            $r = rand(0, 255);
            $g = rand(0, 255);
            $b = rand(0, 255);
            $color = "rgb($r,$g,$b)";
            return $color;
        }
        ?>
        <p>Recarga la pagina y el circulo cambiara a un color al azar.</p>
        <p>Color en RGB: <?= color() ?></p>
        <svg height="100" width="100">
        <circle cx="50" cy="50" r="40" stroke="black" stroke-width="3" fill="<?= color() ?>" />
        </svg>
    </body>
</html>
