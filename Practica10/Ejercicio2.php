<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 10 Ejercicio 2</title>
    </head>
    <body>
        <?php
        $numero = rand(10, 100);
        ?>
        <p>Longitud: <?= $numero ?></p>
        <svg width="<?= $numero ?>" height="10">
        <rect width="<?= $numero ?>" height="10" style="fill:red;stroke-width:3;stroke:rgb(0,0,0)" />
        </svg>
    </body>
</html>
