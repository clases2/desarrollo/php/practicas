<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 10 Ejercicio 5</title>
    </head>
    <body>
        <?php

        function color() {
            $r = rand(0, 255);
            $g = rand(0, 255);
            $b = rand(0, 255);
            $color = "rgb($r,$g,$b)";
            return $color;
        }
        ?>
        <p>Recarga la pagina y el circulo cambiara a un color al azar.</p>
        <p>Color en RGB: <?= color() ?></p>
        <div style="width:50px;height:50px;border-radius:50%;background-color: <?= color() ?>"></div>
    </body>
</html>
