<!DOCTYPE html>
<!--
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHPWebPage.php to edit this template
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 2 Ejercicio 6</title>
    </head>
    <body>
        <?php
        $alumnos = ["Ramon", "Jose", "Pepe", "Ana"];
        for ($c = 0; $c < count($alumnos); $c++) {
            echo "$alumnos[$c]<br>";
        }

        echo "<br>";

        foreach ($alumnos as $value) {
            echo "$value <br>";
        }
        ?>
    </body>
</html>
