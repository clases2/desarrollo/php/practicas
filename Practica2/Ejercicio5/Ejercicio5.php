<!DOCTYPE html>
<!--
Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHPWebPage.php to edit this template
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 2 Ejercicio 5</title>
    </head>
    <body>
        <?php
        $alumno1 = "Ramon";
        $alumno2 = "Jose";
        $alumno3 = "Pepe";
        $alumno4 = "Ana";

        echo $alumno1;
        echo $alumno2;
        echo "<br>";
        echo "$alumno3";
        echo "<br>";
        echo $alumno4;
        ?>
        <div>
            <?php
            echo "$alumno1<br>$alumno2<br>$alumno3<br>$alumno4";
            ?>
        </div>
    </body>
</html>
