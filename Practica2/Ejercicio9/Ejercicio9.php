<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 2 Ejercicio 9</title>
    </head>
    <body>
        <?php
        $nombreCompleto = "Ramon Abramo - ramon@alpeformacion.es";

        for ($c = 0; $c < strlen($nombreCompleto); $c++) {
            echo "$nombreCompleto[$c]<br>";
        }


        //$nombreCompleto=explode(" ",$nombreCompleto); Esta funcion separa el string con el conicional, en este caso hace un array con cada palabra

        $nombreCompleto = str_split($nombreCompleto);
        foreach ($nombreCompleto as $value) {
            echo "$value <br>";
        }
        ?>
    </body>
</html>
