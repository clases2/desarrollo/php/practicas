<!DOCTYPE html>

<html>

    <head>
        <meta charset="UTF-8">
        <title>Practica 11 Ejercicio 1</title>
    </head>

    <body>
        <?php
        //Genero todos los valores al azar usando la funcion rand().
        $valores = rand(10, 20);
        $minimo = rand(1, 100);
        $maximo = rand(1, 100);

        //Controlo el valor maximo.
        while ($minimo >= $maximo) {
            $maximo = rand(1, 100);
        }

        /**
         * Funcion que crea un array y la llena de numeros aleatorios.
         * @param int $n1 Numero de variables en el array.
         * @param int $n2 Valor minimo posible.
         * @param int $n3 Valor maximo posible.
         * @return array El array de numeros creado.
         */
        function CrearArray($n1, $n2, $n3) {
            $n4 = [];
            for ($i = 0; $i < $n1; $i++) {
                array_push($n4, rand($n2, $n3));
            }
            return $n4;
        }

        //Le doy los argumentos a la funcion y la dejo hacer su magia.
        $array = CrearArray($valores, $minimo, $maximo);
        //Los arreglo de menor a mayor usando la funcion sort().
        sort($array);
        //Convierto el array ordenado en un string para poder imprimirlo.
        $imprimir = implode(", ", $array);

        echo "El numero de valores del array es $valores, el valor maximo es $maximo y el minimo es $minimo.<br>El array es: $imprimir.";
        ?>
        <div>
            <p>El programa crea un array y lo llena con numeros aleatorios a partir de una funcion con 3 argumentos, el valor minimo, maximo y el numero de valores.</p>
        </div>
    </body>

</html>