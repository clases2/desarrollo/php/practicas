<!DOCTYPE html>

<html>

    <head>
        <meta charset="UTF-8">
        <title>Practica 11 Ejercicio 4</title>
    </head>

    <body>
        <div>
            <p>El programa crea un array y lo llena con valores aleatorios a partir de una funcion.</p>
        </div>
        <?php
        $aleatorio = rand(5, 15);

        /**
         * Funcion que genera colores.
         * @param int $numero El numero de colores a generar.
         * @bool $almohadilla=true Con valor true nos indica que coloquemos la almohadilla.
         * @return array $colores Los colores solicitados en un array de cadenas.
         */
        function generaColores($numero, $almohadilla = true) {
            $colores = [];
            for ($n = 0; $n < $numero; $n++) {
                $c = 0;
                $limite = 6;
                $colores[$n] = "";
                if ($almohadilla) {
                    $colores[$n] = "#";
                    $limite = 7;
                }
                for ($c = 1; $c < $limite; $c++) {
                    $colores[$n] .= dechex(mt_rand(0, 15));
                }
            }
            return $colores;
        }

        $colores = generaColores($aleatorio);
        echo "El numero de colores en el array es $aleatorio, y los colores generados fueron los siguientes:<br>";
        foreach ($colores as $key => $value) {
            $z = $key + 1;
            echo "$z. $value.<br>";
        }
        $colores = generaColores($aleatorio, false);
        echo "El numero de colores en el array es $aleatorio, el argumento opcional es false y los colores generados fueron los siguientes:<br>";
        foreach ($colores as $key => $value) {
            $z = $key + 1;
            echo "$z. $value.<br>";
        }
        ?>
    </body>

</html>