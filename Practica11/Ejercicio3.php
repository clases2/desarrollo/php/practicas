<!DOCTYPE html>

<html>

    <head>
        <meta charset="UTF-8">
        <title>Practica 11 Ejercicio 3</title>
    </head>

    <body>
        <?php
        $valores = rand(5, 15);
        $colores = [];

        /**
         * Esta funcion genera una serie de colores aleatorios.
         * @param int $n1 Numero de colores que se quieren generar.
         * @param array $n2 El array donde se van a guardar los colores.
         */
        function CrearColores($n1, &$n2) {
            for ($i = 0; $i < $n1; $i++) {
                $r = rand(0, 255);
                $g = rand(0, 255);
                $b = rand(0, 255);
                $rgb = "rgb($r,$g,$b)";
                array_push($n2, $rgb);
            }
        }

        CrearColores($valores, $colores);
        echo "El numero de colores en el array es $valores, y los colores generados fueron los siguientes:<br>";
        foreach ($colores as $key => $value) {
            $z = $key + 1;
            echo "$z. $value.<br>";
        }
        ?>
        <div>
            <p>El programa crea un array y lo llena con numeros aleatorios a partir de una funcion con 3 argumentos, el valor minimo, maximo y el numero de valores.</p>
        </div>
    </body>

</html>