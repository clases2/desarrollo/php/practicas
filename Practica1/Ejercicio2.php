<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejercicio 2 de la Hoja de Ejercicios 1 de PHP</title>
    </head>
    <body>
        <table width="100%" border="1">
            <tr>
                <td>
                    <?php
                    // Cuando utiliceis la instruccion echo puedes utilizar als comillas dobles o simples
                    echo 'Este texto quiero que lo escribas usando la funcion echo de php';
                    ?>
                </td>
                <td>Aqui debe colocar un texto directamente en HTML</td>
            </tr>
            <tr>
                <td>
                    <?php
                    // Cuando utiliceis la instruccion echo puedes utilizar als comillas dobles o simples
                    print 'Este texto quiero que lo escribas utilizando la funcion print de php';
                    ?>
                </td>
                <td>
                    <?php
                    echo 'Academia Alpe';
                    ?>
                </td>
            </tr>
        </table>
    </body>
</html>
