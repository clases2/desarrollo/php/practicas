<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 4 Ejercicio 12</title>
    </head>
    <body>
        <?php
        $a = [8, 6];

        if ($a[0] > $a[1]) {
            print "a es mayor que b<br>";
            $a[1] = $a[0];
        }


        if ($a[0] > $a[1]):
            print "A es mayor que B<br>";
        endif;

        if ($a[0] > $a[1]) {
            print "a es mayor que b<br>";
        } elseif ($a[0] == $a[1]) {
            print "a es igual que b<br>";
        } else {
            print "b es mayor que a<br>";
        }

        if ($a[0] > $a[1]):
            print "A es mayor que B<br>";
            print "...";
        elseif ($a[0] == $a[1]):
            print"A es igual a B<br>";
            print "!!!";
        else:
            print "B es mayor que A<br>";
        endif;
        ?>
    </body>
</html>
