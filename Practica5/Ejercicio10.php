<?php
require './Ejercicio8_Libreria.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 5 Ejercicio 10</title>
    </head>
    <body>
        <p>Programa que lee cuatro notas e imprime la media.</p>
        <?php
        $numero1 = 0;
        $numero2 = 0;
        $numero3 = 0;
        $numero4 = 0;

        if (isset($_GET["boton"])) {
            $numero1 = $_GET["numero1"];
            $numero2 = $_GET["numero2"];
            $numero3 = $_GET["numero3"];
            $numero4 = $_GET["numero4"];

            $n1 = $numero1;
            $n2 = $numero2;
            $n3 = $numero3;
            $n4 = $numero4;

            render2($numero1, $numero2, $numero3, $numero4);
            echo "La nota media es: " . notaMedia($n1, $n2, $n3, $n4);
        } else {
            render2($numero1, $numero2, $numero3, $numero4);
        }
        ?>
    </body>
</html>
