<?php
$numero = 0;
$numero1 = 0;
$salida = '';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 5 Ejercicio 1</title>
    </head>
    <body>
        <form method="GET">
            <div>
                <p>Introduce un numero: <input type="number" name="numero" value="<?= $numero ?>"/></p>
                <p>Introduce otro numero: <input type="number" name="numero1" value="<?= $numero1 ?>"/></p> 
            </div>
            <button name="enviar">Enviar</button>
        </form>
        <?php
        if (isset($_GET["enviar"])) {
            $numero = $_GET["numero"];
            $numero1 = $_GET["numero1"];

            if ($numero > $numero1) {
                $salida = 'Tipo 1';
            } elseif ($numero == $numero1) {
                $salida = 'Tipo 2';
            } else {
                $salida = 'Tipo 3';
                if ($numero1 > 10) {
                    $salida = 'Tipo 31';
                } else {
                    $salida = 'Tipo 32';
                }
            }

            echo "<p>El resultado es: $salida</p>";
        }
        ?>
    </body>
</html>
