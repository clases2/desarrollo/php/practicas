<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 5 Ejercicio 6</title>
    </head>
    <body>
        <p>El programa muestra los numeros del 1 al 100 usando un for</p>
        <p style="width: 300px">
            <?php
            for ($c = 1; $c <= 100; $c++) {
                echo "$c, ";
            }
            ?>  
        </p>
        
    </body>
</html>
