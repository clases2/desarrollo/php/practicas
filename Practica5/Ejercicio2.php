<?php
$n1 = 0;
$n2 = 0;
$n3 = 0;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 5 Ejercicio 2</title>
    </head>
    <body>
        <form method="GET">
            <div>
                <p>Introduce el primer numero: <input type="number" name="numero1" /></p>
                <p>Introduce el segundo numero: <input type="number" name="numero2" /></p>
                <p>Introduce el tercer numero: <input type="number" name="numero3" /></p>
            </div>
            <button name="enviar">Enviar</button>
        </form>
        <?php
        if (isset($_GET["enviar"])) {
            $n1 = $_GET["numero1"];
            $n2 = $_GET["numero2"];
            $n3 = $_GET["numero3"];

            if ($n1 == $n2) {
                echo 'No son distintos.';
            } elseif ($n1 == $n3) {
                echo 'No son distintos.';
            } elseif ($n2 == $n3) {
                echo 'No son distintos';
            } else {
                echo 'Son distintos';
            }
        }
        ?>
    </body>
</html>
