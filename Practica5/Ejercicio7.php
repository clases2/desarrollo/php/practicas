<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 5 Ejercicio 7</title>
    </head>
    <body>
        <p>El programa muestra los numeros del 1 al 100 usando un while</p>
        <p style="width: 300px">
            <?php
            $c = 1;
            while ($c <= 100) {
                echo "$c, ";
                $c++;
            }
            ?>  
        </p>

    </body>
</html>
