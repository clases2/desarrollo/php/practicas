<?php
require './Ejercicio8_Libreria.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 5 Ejercicio 9</title>
    </head>
    <body>
        <p>Programa que suma todos los numeros impares dese 0 hasta el numero que escribas.</p>
        <?php
        $numero = 0;
        if (isset($_GET["boton"])) {
            $numero = $_GET["numero"];
            $c = $numero;
            render($numero);
            sumaImpar($c);
        } else {
            render($numero);
        }
        ?>
    </body>
</html>
