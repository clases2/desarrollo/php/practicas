<?php

function render($numero) {
    include './Ejercicio8_Vista.php';
}

function dowhile($c) {
    echo "<div style='width: 300px'>";
    do {
        echo "$c, ";
        $c -= 2;
    } while ($c > 10);
    echo "</div>";
}

function sumaImpar($c) {
    $acumulador = [];
    $resultado = 0;

    echo "<div style='width: 300px'>";

    for ($i = 1; $i < $c; $i += 2) {
        array_push($acumulador, $i);
        $resultado += $i;
    }

    echo implode('+ ', $acumulador) . " = " . $resultado;
    echo "</div>";
}

function notaMedia($n1, $n2, $n3, $n4) {
    $media = ($n1 + $n2 + $n3 + $n4) / 4;
    return $media;
}

function render2($numero1, $numero2, $numero3, $numero4) {
    include './Ejercicio10_Vista.php';
}

function notaMedia2($notas) {
    $acumulador = 0;
    $resultado = 0;
    foreach ($notas as $value) {
        $acumulador += $value;
    }
    $resultado = $acumulador / count($notas);
    return $resultado;
}
