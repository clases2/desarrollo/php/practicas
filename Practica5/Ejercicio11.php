<?php
include './Ejercicio8_Libreria.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 5 Ejercicio 11</title>
    </head>
    <body>
        <p>Programa que lee un numero y crea formularios en base a ese numero, los formularios leen una nota cada uno e imprimen una media.</p>
        <?php
        $numero = 0;
        $notas = [];

        if (isset($_GET["boton"])) {
            //Se presiona el boton por primera vez y cargan los formularios.
            $numero = $_GET["numero"];
            include "./Ejercicio11_Vista2.php";
        } else if (isset($_GET["boton2"])) {
            //Se presiona el segundo boton y carga el resultado.
            $notas = $_GET["notas"];
            echo "La nota media es de: " . notaMedia2($notas);
        } else {
            //Inicio del programa, no se ha presionado ningun boton.
            include "./Ejercicio11_Vista1.php";
        }
        ?>
    </body>
</html>
