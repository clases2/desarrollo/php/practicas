<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Practica 5 Ejercicio 3</title>
    </head>
    <body>
        <form method="GET">
            <div>
                <p>Introduce un numero: <input type="number" name="numero"/></p>
            </div>
            <button name="enviar">Enviar</button>
        </form>

        <?php
        if (isset($_GET["enviar"])) {
            $numero = $_GET["numero"];
            $salida = '';

            switch ($numero) {
                case 1:
                    $salida = 'Lunes.';
                    break;
                case 2:
                    $salida = 'Martes.';
                    break;
                case 3:
                    $salida = 'Miercoles.';
                    break;
                case 4:
                    $salida = 'Jueves.';
                    break;
                case 5:
                    $salida = 'Viernes.';
                    break;
                case 6:
                    $salida = 'Sabado.';
                    break;
                case 7:
                    $salida = 'Domingo.';
                    break;
                default:
                    $salida = 'No valido';
            }

            echo "El dia correspondiente al numero introducido es: $salida";
        }
        ?> 
    </body>
</html>
